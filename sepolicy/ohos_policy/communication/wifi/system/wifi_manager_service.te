# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

allow wifi_manager_service dev_unix_file:sock_file write;
allow wifi_manager_service accesstoken_service:binder { call };
allow wifi_manager_service data_service_el1_file:dir { add_name remove_name search write create };
allow wifi_manager_service data_service_el1_file:file { create getattr ioctl lock open read setattr unlink write };
allow wifi_manager_service data_file:dir { search };
allow wifi_manager_service data_service_el1_file:sock_file { write };
allow wifi_manager_service accessibility_param:file { read };
allow wifi_manager_service dev_unix_socket:dir { search };
allow wifi_manager_service foundation:binder { call transfer };
allow wifi_manager_service netmanager:binder { call transfer };
allow wifi_manager_service node:udp_socket { node_bind };
allow wifi_manager_service port:udp_socket { name_bind };
allow wifi_manager_service sa_accesstoken_manager_service:samgr_class { get };
allow wifi_manager_service netsysnative:binder { call };
allow wifi_manager_service sa_foundation_cesfwk_service:samgr_class { get };
allow wifi_manager_service sa_net_conn_manager:samgr_class { get };
allow wifi_manager_service sa_wifi_device_ability:samgr_class { add };
allow wifi_manager_service sa_wifi_hotspot_ability:samgr_class { add };
allow wifi_manager_service sa_wifi_p2p_ability:samgr_class { add };
allow wifi_manager_service sa_wifi_p2p_ability:samgr_class { get };
allow wifi_manager_service sa_wifi_scan_ability:samgr_class { add };
allow wifi_manager_service softbus_server:binder { call transfer };
allow wifi_manager_service system_bin_file:dir { search };
allow wifi_manager_service system_bin_file:file { execute execute_no_trans map read open };
allow wifi_manager_service wifi_hal_service:unix_stream_socket { connectto };
allow wifi_manager_service sa_netsys_native_manager:samgr_class { get };
allow wifi_manager_service wifi_manager_service:netlink_route_socket { create nlmsg_read read write };
allow wifi_manager_service wifi_manager_service:packet_socket { bind create read write };
allow wifi_manager_service wifi_manager_service:udp_socket { bind create ioctl setopt getopt read write getattr };
allow wifi_manager_service wifi_manager_service:unix_dgram_socket { ioctl };
allow wifi_manager_service data_service_file:dir { search };
allow wifi_manager_service normal_hap_attr:binder { call transfer };
allow wifi_manager_service system_core_hap_attr:binder { call transfer };
allow wifi_manager_service system_basic_hap_attr:binder { call transfer };
allow wifi_manager_service sa_foundation_appms:samgr_class { get };
allow wifi_manager_service kernel:system { module_request };
allow wifi_manager_service musl_param:file { read };
allow wifi_manager_service sa_huks_service:samgr_class { get };
allow wifi_manager_service sa_cert_manager_service:samgr_class { get };
allow wifi_manager_service cert_manager_service:binder { call };
allow wifi_manager_service huks_service:binder { call };
allowxperm wifi_manager_service data_service_el1_file:file ioctl { 0x5413 };
allowxperm wifi_manager_service wifi_manager_service:udp_socket ioctl { 0x8910 0x890B 0x8913 0x8914 0x8915 0x8916 0x891b 0x891c 0x8927 0x8933 0x89f1 };
allowxperm wifi_manager_service wifi_manager_service:unix_dgram_socket ioctl { 0x8910 };
allow wifi_manager_service musl_param:file { open };
allow wifi_manager_service musl_param:file { map };
allow wifi_manager_service distributeddata:binder { call transfer };
allow wifi_manager_service distributeddata:fd { use };
allow wifi_manager_service sa_dataobs_mgr_service_service:samgr_class { get };
allow wifi_manager_service sa_distributeddata_service:samgr_class { get };
allow wifi_manager_service sa_foundation_abilityms:samgr_class { get };
allow wifi_manager_service sa_wifi_device_ability:samgr_class { get };
allow wifi_manager_service sys_file:file { read open };
# avc:  denied  { read write } for  pid=7931 comm="sa_main" path="/dev/console" dev="tmpfs" ino=40 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:dev_console_file:s0 tclass=chr_file permissive=1
allow wifi_manager_service dev_console_file:chr_file { read write };

# avc:  denied  { getattr } for  pid=7931 comm="wifi_manager_se" path="/sys/devices/system/cpu/online" dev="sysfs" ino=4917 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sysfs_devices_system_cpu:s0 tclass=file permissive=1
# avc:  denied  { open } for  pid=7931 comm="wifi_manager_se" path="/sys/devices/system/cpu/online" dev="sysfs" ino=4917 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sysfs_devices_system_cpu:s0 tclass=file permissive=1
# avc:  denied  { read } for  pid=7931 comm="wifi_manager_se" name="online" dev="sysfs" ino=4917 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sysfs_devices_system_cpu:s0 tclass=file permissive=1
allow wifi_manager_service sysfs_devices_system_cpu:file { getattr open read };

# avc:  denied  { open } for  pid=860 comm="AutoStartThread" path="/sys/class/net" dev="sysfs" ino=14626 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sysfs_net:s0 tclass=dir permissive=1
# avc:  denied  { read } for  pid=860 comm="AutoStartThread" name="net" dev="sysfs" ino=14626 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sysfs_net:s0 tclass=dir permissive=1
allow wifi_manager_service sysfs_net:dir { open read };


# avc:  denied  { getopt } for  pid=7931 comm="RunHandleThread" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:wifi_manager_service:s0 tclass=unix_dgram_socket permissive=1
# avc:  denied  { setopt } for  pid=7931 comm="RunHandleThread" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:wifi_manager_service:s0 tclass=unix_dgram_socket permissive=1
allow wifi_manager_service wifi_manager_service:unix_dgram_socket { getopt setopt };

# avc:  denied  { connectto } for  pid=1828 comm="GetHostThread" path="/dev/unix/socket/dnsproxyd" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:netsysnative:s0 tclass=unix_stream_socket permissive=1
allow wifi_manager_service netsysnative:unix_stream_socket { connectto };

# avc:  denied  { connect } for  pid=1828 comm="NetCheckThread" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:wifi_manager_service:s0 tclass=tcp_socket permissive=1
# avc:  denied  { create } for  pid=1828 comm="NetCheckThread" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:wifi_manager_service:s0 tclass=tcp_socket permissive=1
allow wifi_manager_service wifi_manager_service:tcp_socket { connect create getopt read write setopt getattr bind };
allow wifi_manager_service port:tcp_socket { name_connect };

# avc:  denied  { get } for service=4010 pid=1814 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sa_telephony_tel_core_service:s0 tclass=samgr_class permissive=0
allow wifi_manager_service sa_telephony_tel_core_service:samgr_class { get };
 
# avc:  denied  { call } for  pid=3727 comm="RunHandleThread" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:telephony_sa:s0 tclass=binder permissive=0
allow wifi_manager_service telephony_sa:binder { call };

# avc:  denied  { transfer } for  pid=2121 comm="IPC_2_2419" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:locationhub:s0 tclass=binder permissive=1
allow wifi_manager_service locationhub:binder { transfer };

# avc:  denied  { get } for  service=3301 pid=1449 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sa_foundation_powermgr_service:s0 tclass=samgr_class permissive=0
allow wifi_manager_service sa_foundation_powermgr_service:samgr_class { get };

allow wifi_manager_service wifi_hal_service:binder { transfer call };

allow wifi_manager_service sa_dhcp_client:samgr_class { add get };
allow wifi_manager_service sa_dhcp_server:samgr_class { add get };

allow wifi_manager_service normal_hap_attr:fd { use };
allow wifi_manager_service sa_msdp_movement_service:samgr_class { get };

allow wifi_manager_service sa_device_service_manager:samgr_class { get };
allow wifi_manager_service hdf_devmgr:binder { call };
allow wifi_manager_service hdf_wlan_interface_service:hdf_devmgr_class { get };
allow wifi_manager_service hdf_device_manager:hdf_devmgr_class { get };
allow wifi_manager_service hdf_wpa_interface_service:hdf_devmgr_class { get };
allow wifi_manager_service sa_time_service:samgr_class { get };
allow wifi_manager_service sa_foundation_battery_service:samgr_class { get };
allow wifi_manager_service time_service:binder { call };

# avc:  denied  { get } for service=4010 pid=1814 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sa_accountmgr:s0 tclass=samgr_class permissive=0
allow wifi_manager_service sa_accountmgr:samgr_class { get };

# avc:  denied  { call } for  pid=599 comm="IPC_1_2526" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:accountmgr:s0 tclass=binder permissive=0
# avc:  denied  { transfer } for  pid=2121 comm="IPC_2_2419" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:accountmgr:s0 tclass=binder permissive=0
allow wifi_manager_service accountmgr:binder { call transfer };

allow wifi_manager_service wifi_host:binder { call transfer };
allow wifi_manager_service wifi_host:unix_dgram_socket { sendto };
allow wifi_manager_service data_local:dir { search };

allow wifi_manager_service dev_unix_socket:sock_file { write };
allow wifi_manager_service paramservice_socket:sock_file { write };

allow wifi_manager_service hdf_hostapd_interface_service:hdf_devmgr_class { get };
allow wifi_manager_service dev_block_volfile:dir { search };
allow wifi_manager_service kernel:unix_stream_socket { connectto };
allow wifi_manager_service data_vendor:dir { search };

allow wifi_manager_service persist_param:parameter_service { set };
allow wifi_manager_service dev_block_file:dir { search };
allow wifi_manager_service dev_block_file:lnk_file { read };

# avc:  denied  { getattr } for  pid=1419 comm="RunHandleThread" laddr=7.246.161.199 lport=52412 faddr=121.14.84.231 fport=80 scontext=u:r:wifi_manager_service:s0 tcontext=u:r:wifi_manager_service:s0 tclass=tcp_socket permissive=1
allow wifi_manager_service wifi_manager_service:tcp_socket { getattr setopt };

# avc:  denied  { call } for  pid=1386 comm="OS_cesComLstnr" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:wifi_host:s0 tclass=binder permissive=1
allow wifi_manager_service wifi_host:binder { call transfer };


# avc:  denied  { get } for service=5100 pid=1367 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:sa_device_service_manager:s0 tclass=samgr_class permissive=1
allow wifi_manager_service sa_device_service_manager:samgr_class { get };


# avc:  denied  { get } for service=hdf_device_manager pid=1365 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:hdf_device_manager:s0 tclass=hdf_devmgr_class permissive=1
allow wifi_manager_service hdf_device_manager:hdf_devmgr_class { get };


# avc:  denied  { get } for service=wpa_interface_service pid=1367 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:hdf_wpa_interface_service:s0 tclass=hdf_devmgr_class permissive=1
allow wifi_manager_service hdf_wpa_interface_service:hdf_devmgr_class { get };

# avc:  denied  { open } for  pid=2538 comm="sh" path="/dev/tty" dev="tmpfs" ino=112 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:tty_device:s0 tclass=chr_file permissive=1
allow wifi_manager_service tty_device:chr_file { read write open };

# avc:  denied  { use } for  pid=1353 comm="RunHandleThread" path="/dev/ashmem" dev="tmpfs" ino=615 scontext=u:r:wifi_manager_service:s0 tcontext=u:r:normal_hap:s0 tclass=fd permissive=1
allow wifi_manager_service normal_hap:fd { use };


# avc:  denied  { transfer } for  pid=1359 comm="wifi_manager_se" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:hiview:s0 tclass=binder permissive=1
allow wifi_manager_service hiview:binder { transfer };

# avc:  denied  { search } for  pid=6428 comm="sh" name="local" dev="sdd91" ino=3161 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:data_local:s0 tclass=dir permissive=1
allow wifi_manager_service data_local:dir { search };

# avc:  denied  { read } for  pid=6535 comm="sh" name="cp" dev="sdd86" ino=375 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:system_bin_file:s0 tclass=lnk_file permissive=1
allow wifi_manager_service system_bin_file:lnk_file { read };

# avc:  denied  { getattr } for  pid=5751 comm="sh" path="/system/bin/toybox" dev="sdd86" ino=647 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:system_bin_file:s0 tclass=file permissive=1
allow wifi_manager_service system_bin_file:file { getattr };

# avc:  denied  { getattr } for  pid=6460 comm="cp" path="/data/service/el1/public/wifi/wpa_supplicant" dev="sdd91" ino=3363 scontext=u:r:wifi_manager_service:s0 tcontext=u:object_r:data_service_el1_file:s0 tclass=dir permissive=1
allow wifi_manager_service data_service_el1_file:dir { getattr };


# avc:  denied  { call } for  pid=1376 comm="RunHandleThread" scontext=u:r:wifi_manager_service:s0 tcontext=u:r:hdf_devmgr:s0 tclass=binder permissive=1
allow wifi_manager_service hdf_devmgr:binder { call };
